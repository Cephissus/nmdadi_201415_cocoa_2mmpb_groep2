##Briefing, Analyse en concept
###1. Wat wil de klant?
De klant wenst een webapplicatie om taken in te kunnen noteren. In deze applicatie wil de gebruiker bepaalde taken kunnen ingeven, ze in lijsten kunnen zetten en er bepaalde datums aan vast kunnen hangen.
Bovendien wil de klant niet alleen de vaste lijsten kunnen gebruiken, hij wil ook zelfgekozen lijsten kunnen maken waar hij zijn taken in kan noteren.
###2. Wat is de boodschap?
De boodschap is een nette simplistische user-interface. De gebruiker moet genoeg vrijheid krijgen over hoe hij zijn taken beheert, terwijl hij toch 'vasthangt' aan ons simpele maar elegante design.
###3. Wie is het doelpubliek?
* Demografisch: Van 14 jaar tot 90 jaar, in principe internet- en smartphonegebruikers in het algemeen.
* Psychografisch: Mensen die graag hun taken ordenen
* Technografisch: Iedereen die af en toe op internet actief is, hoeft niet een grote technologie-interesse te hebben.
###4. Welke informatie is er voorhanden?
Er is geen informatie voorhanden. We beginnen vanaf nul met uitzondering van een app-template.
###5. Wat is de stand van zaken?
Er zijn al zeer veel webapps die zich focussen op het sorteren van taken. Wij willen echter met een goed design en een simpele app een gemakkelijke user-experience creëren voor onze gebruikers.
###6. Keuze technologie
#####Front-end webapplicatie aan de hand van:
* HTML5
* CSS3
* Javascript
###7. Tijdspanne
De web-applicatie moet ingeleverd worden op 29 november 2014.

# PERSONA'S #

## Dirk Dewaele 
* Leeftijd: 33
* Beroep: Ondernemer
* Status: Getrouwd
* Locatie: Gent, België

*Bio: Dirk is een jonge dertiger met een eigen bedrijf.
Hij is getrouwd en heeft samen met zijn vrouw twee kinderen, een jongen van 4 jaar en 
meisje van 7 jaar.*


### Motivaties
1. Succes
1. Groei
1. Sociaal

### Doel
1. Industrieel netwerk uitbreiden.
2. Eigen bedrijf laten groeien.

### Frustraties
1. Trage downloadsnelheden
2. Computer crashes
3. Slechte communicatie

---

## Jules Vermeeren 
* Leeftijd: 89
* Beroep: Gepensioneerd
* Status: Weduwnaar
* Locatie: Waregem, België

*Bio: Jules is een militair op pensioen.
5 jaar geleden verloor hij zijn vrouw door ouderdom.
Hij laat zich graag omringen door leeftijdsgenoten in zijn stamkroeg. Op zondag komen zijn twee dochters met hun kinderen op wekelijkse visite.*


### Motivaties
1. Succes
1. Groei
1. Macht

### Doel
1. Genieten van het leven.

### Frustraties
1. Onverdraagzaamheid
2. Leugens
3. Onduidelijkheid

---

## Lieza Verstraete 
* Leeftijd: 21
* Beroep: Student
* Status: In een relatie
* Locatie: Leuven, België

*Bio: Lieza is een student in Leuven. 
Ze studeert verder om haar master te behalen in de verpleegkunde.
Verder is Lieza dol op reizen en gaat ze graag eens weg met vrienden.*


### Motivaties
1. Succes
1. Groei
1. Sociaal

### Doel
1. Behalen van een master-diploma.
2. Inzetten voor het goede doel.

### Frustraties
1. Afhankelijkheid
2. Ontrouwe personen
3. Vertragingen bij het openbaar vervoer

---






