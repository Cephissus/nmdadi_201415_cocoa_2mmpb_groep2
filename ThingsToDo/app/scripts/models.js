/**
 * Created by Administrator on 5/11/14.
*/
/*
Object Literal for Category
 */
/*var Category = {
    "id":-1,
    "name":"Philippe",
    "mobile":32487532698,
    "email":"philippe.depauw@arteveldehs.be",
    "created":new Date().getTime(),
    "updated":null,
    "deleted":null
}*/
/*
 Object constructor for Category (-> class)
 Public properties, methods
 */
function Category(){
    this.id;
    this.firstname;
    this.surname;
    this.mobile;
    this.email;
    this.avatar = null;
    this.created;
    this.updated = null;
    this.deleted = null;//soft-delete

    this.contactgroupId;//0..1 relation to the ContactGroup
}
/*
 Object constructor for ContactGroup (-> class)
 Public properties, methods
 */
function ContactGroup(){
    this.id;
    this.name;
    this.description;
    this.created;
    this.updated;
    this.deleted;//soft-delete
}
/*
 Object constructor for Course (-> class)
 Public properties, methods
 */
/*function Course(){
    this.id;
    this.name;
    this.synopsis;
    this.fromdate;
    this.todate;
    this.place;
    this.created;
    this.updated;
    this.deleted;
    this.toString = function(){
        return this.name;
    }
}*/