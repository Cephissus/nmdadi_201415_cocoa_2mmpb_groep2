
(function(){
    var App = {
        init:function(){
            //Create the ContactDBContext object via a clone
            this.contactDBContext = ContactDBContext;
            this.contactDBContext.init('dds.contactapp');//Initialize the ContactDBContext
            //Cache the most important elements
            this.cacheElements();
            //Bind Events to the most important elements
            this.bindEvents();
            //Setup the routes
            this.setupRoutes();
            //Render the interface
            this.render();
        },
        setupRoutes:function(){
            this.router = crossroads;//Clone the crossroads object
            this.hash = hasher;//Clone the hasher object

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Crossroads settings
            var homeRoute = this.router.addRoute('/',function(){
                //Set the active page where id is equal to the route
                self.setActivePage('contacts');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('contacts');
            });
            var sectionRoute = this.router.addRoute('/{section}');//Add the section route to crossroads
            sectionRoute.matched.add(function(section){
                //Set the active page where id is equal to the route
                self.setActivePage(section);
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink(section);
            });//Hash matches to section route
            var categoryDetailsRoute = this.router.addRoute('/contact-details/{id}');//Add the route to crossroads
            categoryDetailsRoute.matched.add(function(id){
                var contact = self.contactDBContext.getContactById(id);
                self.renderCategoryDetails(contact);

                //Set the active page where id is equal to the route
                self.setActivePage('contact-details');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('contact-details');

            });//Hash matches to section route
            //this.router.routed.add(console.log, console);//Log all crossroads events

            //Hash settings
            this.hash.initialized.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse initial hash
            this.hash.changed.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse changes in the hash
            this.hash.init();//Start listening to the hashes
        },
        cacheElements:function(){
            this.formCategoryCreate = document.querySelector('#formContactCreate');
            this.categoriesList = document.querySelector('#contacts-list');
            this.categoryDetails = document.querySelector('#contact-details-content');
            this.createCategory = document.querySelector('#create-contact');
        },
        bindEvents:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Submit the contact create form --> listen to it!
            this.formCategoryCreate.addEventListener('submit', function(ev){
                //Dismiss the default browser eventlistener for submit event
                ev.preventDefault();

                //Check Validity
                console.log(this.checkValidity());

                //Create a new Category Object
                var category = new Category();
                category.id = Utils.guid();
                category.firstname = this.elements["txtContactFirstName"].value;

                category.created = new Date().getTime();

                //Add Category to the database via Context
                var result = self.contactDBContext.addContact(category);

                //Refresh the user interface -> render
                self.render();

                //Go to list
                self.hash.setHash('contacts');

                return false;
            });

            //Back buttons
            var backButtons = document.querySelectorAll('.back');
            if(backButtons != null && backButtons.length > 0){
                _.each(backButtons, function(backButton){
                    backButton.addEventListener('click', function(ev){
                        ev.preventDefault();

                        window.history.go(-1);//Back to the previous page (HTML5 History API

                        return false;
                    });
                });
            }

            //Create New Category
            this.createCategory.addEventListener('click', function(ev){
                ev.preventDefault();

                self.hash.setHash('contact-create');

                return false;
            });




        },
        render:function(){
            this.renderContacts();//Render the contacts
        },
        renderContacts:function(){
            this.categoriesList.innerHTML = '';
            var html = '';

            var contacts = this.contactDBContext.getContacts();

            if(contacts != null && contacts.length > 0){
                _.each(contacts, function(contact){
                    html += ''
                    + '<article class="contact" data-id=' + contact.id + ' >'
                    + '<span class="name">' +  contact.firstname  + '</span>'
                    +'<button class="fa fa-trash removeCategory">' + '</button>'
                    + '</article>'

                });
            }else{
                html += '<div>There are no categories</div>'
            }

            this.categoriesList.innerHTML = html;

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Register listeners for all the contacts
            var nodes = document.querySelectorAll('.contact');
            if(nodes != null && nodes.length > 0){
                _.each(nodes,function(node){
                    node.addEventListener('click', function(ev){
                        //Get the id of the contact by the dataset property of the data attributes
                        var id = this.dataset.id;
                        self.hash.setHash('contact-details/' + id);
                    });
                });
            }
        },
        renderCategoryDetails:function(category){

            this.categoryDetails.innerHTML = '';
            this.categoryDetails.dataset.id = category.id;

            var html = ''
                + '<section class="category-details-personal">'
                + '<div class="info">'
                + '<span class="name detailname">' +  category.firstname +  '</span>'
                + '<input class="col col-xxs-12" type="text" id="txtNewThingToDo" name="txtNewThingToDo" placeholder="New ThingToDo" required pattern=".{2,}" minlength="2" maxlength="32" />'
                + '<div>' + '<button class="col col-xxs-12 btn btn-primary" type="submit" id="btn">Add ThingToDo</button>' + '</div>'
                + '<div class="listTitle"> <h2>To Do</h2> </div>'
                + '<div class="listTitle"> <h2>Done</h2> </div>'
                + '<ul id="todo">'
                + '</ul>'
                + '<ul id="done">'
                + '</ul>'



            this.categoryDetails.innerHTML = html;


            (function(){
                var input = document.getElementById('txtNewThingToDo');
                var btn = document.getElementById('btn');
                var lists = {
                    todo: document.getElementById('todo'),
                    done: document.getElementById('done')
                };

                var makeTaskHtml = function(str, onCheck){

                    var el = document.createElement('li');
                    var checkbox = document.createElement('input');
                    var label = document.createElement('span');
                    var kruis = document.createElement('button');

                    label.className = 'ThingToDoInList';

                    checkbox.setAttribute("type", "checkbox");
                    checkbox.addEventListener('click', onCheck);
                    label.textContent = str;


                    el.className = 'listItem'
                    label.className = 'ThingToDoInList';
                    kruis.className = 'fa fa-trash removettd';


                    el.appendChild(checkbox);
                    el.appendChild(label);
                    el.appendChild(kruis);



                    return el;

                    this.addThingToDo(str);
                };

                var addTask = function(task){

                    lists.todo.appendChild(task);
                    AppData.thingsToDo.push(task);
                    Utils.store('dds.contactapp', AppData);
                };

                var onCheck = function(event){
                    var task = event.target.parentElement;
                    var list = task.parentElement.id;

                    lists[list === 'done'? 'todo':  'done'].appendChild(task);




                };

                var onInput = function(e){
                    e.preventDefault();
                    var str = input.value.trim();
                    input.value='';
                    input.focus();
                    if(str.length>0){
                        addTask(makeTaskHtml(str, onCheck));




                    }

                };

                btn.addEventListener('click', onInput);
                input.addEventListener('keyup', function(event){
                   var code = event.keyCode;

                    if(code === 13){
                        onInput();
                    }
                });
                input.focus();





            })();






















        },




        //Event Listener: listen to matched section routes (parse)
        onSectionMatch:function(section){
            //Set the active page where id is equal to the route
            this.setActivePage(section);
            //Set the active menuitem where href is equal to the route
            this.setActiveNavigationLink(section);
        },
        setActivePage:function(section){
            //Set the active page where id is equal to the route
            var pages = document.querySelectorAll('.page');
            if(pages != null && pages.length > 0){
                _.each(pages,function(page){
                    if(page.id == section){
                        page.classList.add('active');
                    }else{
                        page.classList.remove('active');
                    }
                });
            }
        },
        setActiveNavigationLink:function(section){
            //Set the active menuitem where href is equal to the route
            var navLinks = document.querySelectorAll('.page .main-navigation ul li a');
            if(navLinks != null && navLinks.length > 0){
                var effLink = '#/' + section;
                _.each(navLinks,function(navLink){
                    if(navLink.getAttribute('href') == effLink){
                        navLink.parentNode.classList.add('active');
                    }else{
                        navLink.parentNode.classList.remove('active');
                    }
                });
            }
        }
    };

    App.init();//Initialize the application
})();