/**
 * Created by Administrator on 5/11/14.
 */
var ContactDBContext = {
    init:function(connString){
        this.connString = connString;
        //Create the AppData object for the Category Application
        this.AppData = {
            "information":{
                "title":"Category Application",
                "version":"1.0",
                "modified":"05-11-2014",
                "author":"Philippe De Pauw - Waterschoot"
            },
            "contacts":[],
            "thingsToDo":[],
            "settings":{}
        };
        //Get Application Data from the localstorage
        if(Utils.store(this.connString) != null){
            this.AppData = Utils.store(this.connString);
        }else{
            Utils.store(this.connString, this.AppData);
        }
    },
    getContacts:function(){
        var contacts = this.AppData.contacts;
        if(contacts == null)
            return null;
        contacts = _.sortBy(contacts, 'name');
        return contacts;
    },

    //added by Bavo

    getThingsToDo: function(){
        var thingsToDo = this.AppData.thingsToDo;
        if(thingsToDo == null)
        return null;
        thingsToDo = _.sortBy(thingsToDo, function(o) { return o.start.dateTime; });
        return thingsToDo;
    },



    getContactById:function(id){
        var contact = _.find(this.AppData.contacts, function(obj){
            return obj.id == id;
        });

        return contact;
    },

    addContact:function(contact){
        this.AppData.contacts.push(contact);
        this.save();

        return 1;
    },
    addThingToDo:function(thingToDo)
    {
        this.AppData.thingsToDo.push(thingToDo);
        this.save();
        return 1;
    },
    updateContact:function(contact){
        var index = _.findIndex(this.AppData.contacts, function(obj){
            return obj.id == contact.id;
        });

        if(index == -1)
            return 0;

        contact.updated = new Date().getTime();
        this.AppData.contacts[index] = contact;
        this.save();

        console.log(this.AppData);

        return 1;
    },

    deleteContact:function(contact){
        return this.deleteContactById(contact.id);
    },


    softDeleteContactById:function(id){
        var contact = this.getContact(id);
        return this.softDeleteContact(contact);
    },
    softDeleteContact:function(contact){
        contact.deleted = new Date().getTime();
        return this.updateContact(contact);
    },
    softUnDeleteContactById:function(id){
        var contact = this.getContact(id);
        return this.softUnDeleteContact(contact);
    },
    softUnDeleteContact:function(contact){
        contact.deleted = null;
        return this.updateContact(contact);
    },
    save:function(){
        Utils.store(this.connString, this.AppData);
    },


};